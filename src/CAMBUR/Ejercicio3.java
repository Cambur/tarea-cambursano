package CAMBUR;

public class Ejercicio3 {

	public static void main(String[] args) {
		System.out.println("TECLA DE ESCAPE\t\tSIGNIFICADO");
		System.out.println("---------------\t\t-----------");
		System.out.println("\\n\t\t\tSIGNIFICA NUEVA LINEA");
		System.out.println("\\t\t\t\tSIGNIFICA UN TAB DE ESPACIO");
		System.out.println("\\\"\t\t\tESCRIBE COMILLAS DOBLES \"");
		System.out.println("\\\\\t\t\tESCRIBE UNA BARRA \\");
		System.out.println("\\\'\t\t\tESCRIBE UNA COMILLA SIMPLE \'");
		
	}

}
